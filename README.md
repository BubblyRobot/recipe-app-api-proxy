# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

List the configuration items that are avaible for the project.

### Environment Variables

* `LISTEN_PORT` - PORT TO LISTEN ON (DEFAULT: `8000`)
* `APP_HOST - HOSTNAME OF THE APP TO FORWARD REQUESTS TO (DEFAULT: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
